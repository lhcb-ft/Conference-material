 Schematic overview of the underlying principles of LHCb's flavour tagging algorithms (PDF version). 
 A TikZ version of the scheme can be found in the Git repository at https://gitlab.cern.ch/lhcb-ft/TikZPics/. 
 Author: Julian Wishahi. 