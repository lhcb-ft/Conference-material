from ROOT import *
from math import *
import os
import sys
import time

gROOT.ProcessLine(".L lhcbStyle.C")
gROOT.SetBatch(True)

mykWhite  = 0
mykBlack  = 1
mykGray    = 920
mykRed    = 632
mykGreen  = 416
mykBlue   = 600
mykYellow = 400
mykMagenta = 616
mykCyan   = 432
mykOrange = 800
mykSpring = 820
mykTeal   = 840
mykAzure   =  850
mykViolet = 880
mykPink   = 900

d_col = {'BuJpsiK':mykGray+1, 'BdJpsiKst':mykBlue, 'BdJpsiKsmumu':mykAzure, 'BdPsi2SKsmumu':mykCyan, 'BdJpsiKsee':mykViolet, 'BsJpsiPhi':mykBlack, 'BsDspi':mykRed, 'incl_Bs':mykRed, 'incl_Bu':mykGray+1}

d_vals = {}

l_modes = ['BuJpsiK','BdJpsiKst','BsJpsiPhi','BsDspi','BdJpsiKsmumu','BdPsi2SKsmumu','incl_Bu','incl_Bs']
l_taggers = ['OS_e', 'OS_mu', 'OS_K', 'OS_VtxC', 'OS_Charm', 'OS_Comb', 'SS_K', 'SS_pi', 'SS_p', 'OS_SS_Comb']
l_years = [2009,2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2017, 2018, 2019, 2020, 2021, 2022]
l_vtypes = ["val","err"]
for mode in l_modes:
   d_vals[mode] = {}
   for tag in l_taggers:
      d_vals[mode][tag] = {}
      for year in l_years:
         d_vals[mode][tag][year] = {}
         for vtype in l_vtypes:
            d_vals[mode][tag][year][vtype] = 0.0


# --------------------------------------------------------------------
# 2009
# --------------------------------------------------------------------
# https://arxiv.org/ftp/arxiv/papers/0912/0912.4179.pdf
# LHCb RoadMap (2009 MC)
d_vals['BuJpsiK']['OS_Comb'][2009]['val'] = 3.35
d_vals['BuJpsiK']['OS_Comb'][2009]['err'] = 0.09
d_vals['BuJpsiK']['OS_SS_Comb'][2009]['val'] = 4.45
d_vals['BuJpsiK']['OS_SS_Comb'][2009]['err'] = 0.10

d_vals['BdJpsiKst']['OS_Comb'][2009]['val'] = 3.45
d_vals['BdJpsiKst']['OS_Comb'][2009]['err'] = 0.10
d_vals['BdJpsiKst']['OS_SS_Comb'][2009]['val'] = 4.52
d_vals['BdJpsiKst']['OS_SS_Comb'][2009]['err'] = 0.11

d_vals['BsJpsiPhi']['OS_Comb'][2009]['val'] = 3.32
d_vals['BsJpsiPhi']['OS_Comb'][2009]['err'] = 0.11
d_vals['BsJpsiPhi']['OS_SS_Comb'][2009]['val'] = 6.23
d_vals['BsJpsiPhi']['OS_SS_Comb'][2009]['err'] = 0.15


# --------------------------------------------------------------------
# 2010
# --------------------------------------------------------------------
# http://cdsweb.cern.ch/record/1328957/files/LHCb-CONF-2011-003.pdf
# LHCb 35 pb-1 (2010 data)
d_vals['BuJpsiK']['OS_Comb'][2010]['val'] = 1.97
d_vals['BuJpsiK']['OS_Comb'][2010]['err'] = 0.31
d_vals['BuJpsiK']['OS_SS_Comb'][2010]['val'] = 2.38
d_vals['BuJpsiK']['OS_SS_Comb'][2010]['err'] = 0.33

d_vals['BdJpsiKst']['OS_Comb'][2010]['val'] = 2.52
d_vals['BdJpsiKst']['OS_Comb'][2010]['err'] = 0.82
d_vals['BdJpsiKst']['OS_SS_Comb'][2010]['val'] = 2.82
d_vals['BdJpsiKst']['OS_SS_Comb'][2010]['err'] = 0.87


# --------------------------------------------------------------------
# 2014
# --------------------------------------------------------------------
# https://twiki.cern.ch/twiki/pub/LHCbPhysics/B2OCTD_Bs2DsK_TD/PAPERDRAFTv6a.pdf
# 1fb-1 Run1 analysis
d_vals['BsDspi']['OS_Comb'][2013]['val'] = 3.75
d_vals['BsDspi']['OS_Comb'][2013]['err'] = 0.10
d_vals['BsDspi']['OS_SS_Comb'][2013]['val'] = 5.07
d_vals['BsDspi']['OS_SS_Comb'][2013]['err'] = 0.30

# https://twiki.cern.ch/twiki/pub/LHCbPhysics/Bd2JpsiKS/LHCb-ANA-2013-046_v2.2.pdf
# 3fb-1 Run1 analysis
d_vals['BdJpsiKsmumu']['OS_SS_Comb'][2013]['val'] = 3.02
d_vals['BdJpsiKsmumu']['OS_SS_Comb'][2013]['err'] = 0.05

# https://arxiv.org/pdf/1304.2600.pdf
# 1fb-1 Run1 analysis
d_vals['BsJpsiPhi']['OS_SS_Comb'][2013]['val'] = 3.33
d_vals['BsJpsiPhi']['OS_SS_Comb'][2013]['err'] = 0.23

# https://twiki.cern.ch/twiki/pub/LHCbPhysics/B2OCTD_Bs2DsK_TD_3fb/LHCb-PAPER-2017-047_v9.pdf
# 3fb-1 Run1 analysis
d_vals['BsDspi']['OS_Comb'][2015]['val'] = 3.55
d_vals['BsDspi']['OS_Comb'][2015]['err'] = 0.33
d_vals['BsDspi']['OS_SS_Comb'][2015]['val'] = 5.80
d_vals['BsDspi']['OS_SS_Comb'][2015]['err'] = 0.25

d_vals['BdPsi2SKsmumu']['OS_SS_Comb'][2015]['val'] = 3.42
d_vals['BdPsi2SKsmumu']['OS_SS_Comb'][2015]['err'] = 0.09
# d_vals['BdJpsiKsee']['OS_SS_Comb'][2015]['val'] = 5.93
# d_vals['BdJpsiKsee']['OS_SS_Comb'][2015]['err'] = 0.29

# https://twiki.cern.ch/twiki/pub/LHCbPhysics/Bs2JpsiPhi/LHCb-ANA-2014-039-20140923.pdf
# 3fb-1 Run1 analysis
d_vals['BsJpsiPhi']['OS_SS_Comb'][2015]['val'] = 3.73
d_vals['BsJpsiPhi']['OS_SS_Comb'][2015]['err'] = 0.15

# --------------------------------------------------------------------
# 2017
# --------------------------------------------------------------------
# https://twiki.cern.ch/twiki/pub/LHCbPhysics/Bs2JpsiPhi2016/Analysis_Note_Phis_v1r1.pdf
# 2016 data Run1 taggers
d_vals['BsJpsiPhi']['OS_SS_Comb'][2016]['val'] = 4.04
d_vals['BsJpsiPhi']['OS_SS_Comb'][2016]['err'] = 0.39


# --------------------------------------------------------------------
# 2018
# --------------------------------------------------------------------
# https://twiki.cern.ch/twiki/pub/LHCbPhysics/Bs2JpsiPhi2016/Analysis_Note_Phis_v2.pdf
# 2015+2016 prelim
d_vals['BsJpsiPhi']['OS_SS_Comb'][2018]['val'] = 4.73
d_vals['BsJpsiPhi']['OS_SS_Comb'][2018]['err'] = 0.34

# https://indico.cern.ch/event/738171/contributions/3046059/attachments/1671677/2681939/FlavourTagging_meeting.pdf
# 2015+2016 prelim
d_vals['BsDspi']['OS_SS_Comb'][2018]['val'] = 7.2
d_vals['BsDspi']['OS_SS_Comb'][2018]['err'] = 0.5

d_vals['incl_Bs']['OS_SS_Comb'][2019]['val'] = 9.6
d_vals['incl_Bs']['OS_SS_Comb'][2019]['err'] = 0.5
d_vals['incl_Bu']['OS_SS_Comb'][2019]['val'] = 7.6
d_vals['incl_Bu']['OS_SS_Comb'][2019]['err'] = 0.06


l_bins = []
for ibin in range(len(l_years)-1):
   l_bins.append(ibin)
   
d_gr = {}
for mode in l_modes:
   d_gr[mode] = {}
   for tag in l_taggers:
      d_gr[mode][tag] = TGraphErrors()
      d_gr[mode][tag].SetName("g%s_%s"%(mode,tag))
      d_gr[mode][tag].SetLineColor(d_col[mode])
      d_gr[mode][tag].SetMarkerColor(d_col[mode])
      #d_gr[mode][tag].SetMarkerStyle(20)
      
    
for tag in l_taggers:
   for mode in l_modes:
      for ibin in l_bins:
         d_gr[mode][tag].SetPoint(ibin, (l_years[ibin]+(float(l_years[ibin+1])-float(l_years[ibin]))/2.0), d_vals[mode][tag][l_years[ibin]]["val"])
         d_gr[mode][tag].SetPointError(ibin, ((float(l_years[ibin+1])-float(l_years[ibin]))/2.0), d_vals[mode][tag][l_years[ibin]]["err"])
            
for tag in l_taggers:
   cv = TCanvas()
   cv.SetFillColor(0)
   m_gr = TMultiGraph()
   for mode in l_modes:
      d_gr[mode][tag].SaveAs("plots/%s_%s_Graph.root"%(mode,tag))
      m_gr.Add(d_gr[mode][tag])
      
   m_gr.Draw("AP")
   m_gr.SetTitle("")
   m_gr.GetXaxis().SetTitle()
   m_gr.GetYaxis().SetTitle("#varepsilon D^{2} (%)")
   
   m_gr.SaveAs("plots/%s_mGraph.root"%tag)
   cv.SaveAs("plots/%s_mGraph_cv.root"%tag)
   cv.SaveAs("plots/%s_mGraph_cv.pdf"%tag)
   
