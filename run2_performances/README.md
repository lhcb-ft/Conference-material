# OS (individual and combined) calibrated performances (2016 $`B^0 \to D^- \pi^+`$)

```
\begin{tabular}{rlllll}
\multicolumn{1}{c}{Tagger} & \multicolumn{1}{c}{$\epsilon$} & \multicolumn{1}{c}{$\omega$} & \multicolumn{1}{c}{$\epsilon \langle D^2 \rangle = \epsilon \left( 1 - 2 \omega \right)^2$} \\ 
OS $\mu$& $(9.150\pm0.053)\%$& $(30.713\pm0.048(\textrm{stat})\pm0.431(\textrm{cal}))\%$& $(1.361\pm0.010(\textrm{stat})\pm0.061(\textrm{cal}))\%$\\
OS $e$& $(4.451\pm0.038)\%$& $(34.038\pm0.072(\textrm{stat})\pm0.600(\textrm{cal}))\%$& $(0.454\pm0.006(\textrm{stat})\pm0.034(\textrm{cal}))\%$\\
OS $K$& $(19.600\pm0.073)\%$& $(37.557\pm0.023(\textrm{stat})\pm0.314(\textrm{cal}))\%$& $(1.214\pm0.006(\textrm{stat})\pm0.061(\textrm{cal}))\%$\\
Vertex Charge& $(20.834\pm0.075)\%$& $(36.994\pm0.025(\textrm{stat})\pm0.307(\textrm{cal}))\%$& $(1.410\pm0.007(\textrm{stat})\pm0.067(\textrm{cal}))\%$\\
OS $c$& $(5.025\pm0.040)\%$& $(34.062\pm0.033(\textrm{stat})\pm0.619(\textrm{cal}))\%$& $(0.511\pm0.005(\textrm{stat})\pm0.040(\textrm{cal}))\%$\\
OS Combination& $(40.154\pm0.090)\%$& $(35.123\pm0.025(\textrm{stat})\pm0.210(\textrm{cal}))\%$& $(3.555\pm0.014(\textrm{stat})\pm0.100(\textrm{cal}))\%$\\
\end{tabular}
```

# SSK calibrated performances (2016 $`B^0_s \to D^-_s \pi^+`$)

```
\begin{tabular}{rlllll}
\multicolumn{1}{c}{Tagger} & \multicolumn{1}{c}{$\epsilon$} & \multicolumn{1}{c}{$\omega$} & \multicolumn{1}{c}{$\epsilon \langle D^2 \rangle = \epsilon \left( 1 - 2 \omega \right)^2$} \\ 
SS $K$& $(68.190\pm0.177)\%$& $(39.667\pm0.039(\textrm{stat})\pm0.505(\textrm{cal}))\%$& $(2.912\pm0.023(\textrm{stat})\pm0.285(\textrm{cal}))\%$\\
\end{tabular}
```


# SS$`\pi`$, SS$`p`$ and combination calibrated performances (2016 $`B^0 \to D^- \pi^+`$)

```
\begin{tabular}{rlllll}
\multicolumn{1}{c}{Tagger} & \multicolumn{1}{c}{$\epsilon$} & \multicolumn{1}{c}{$\omega$} & \multicolumn{1}{c}{$\epsilon \langle D^2 \rangle = \epsilon \left( 1 - 2 \omega \right)^2$} \\
SS $\pi$& $(83.486\pm0.068)\%$& $(42.561\pm0.014(\textrm{stat})\pm0.144(\textrm{cal}))\%$& $(1.848\pm0.007(\textrm{stat})\pm0.072(\textrm{cal}))\%$\\
SS $p$& $(37.767\pm0.089)\%$& $(43.645\pm0.022(\textrm{stat})\pm0.220(\textrm{cal}))\%$& $(0.610\pm0.004(\textrm{stat})\pm0.042(\textrm{cal}))\%$\\
SS Combination& $(87.590\pm0.061)\%$& $(41.787\pm0.015(\textrm{stat})\pm0.141(\textrm{cal}))\%$& $(2.364\pm0.009(\textrm{stat})\pm0.081(\textrm{cal}))\%$\\
\end{tabular}
```